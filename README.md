# Todo App

This is a Todo application written in React-Native for iOS and Android. Expo CLI is used for easier running and testing. 

The Todo app is a simple application, therefore it doesn't require any sophisticated state management. I used a normal React state and a hook to handle that. But in more complex applications we could utilize different tools and structures to make it more maintainable.

Currently the code doesn't follow any specific folder structure, in larger projects it should be.

## Run project

### Step 1: Install dependencies

```bash
$ npm install
```

### Step 2: Run in development mode

```bash
$ npm start
```

### Run tests

```bash
$ npm run test
```

### Run static code quality checker

```bash
$ npm run static-code-quality
```

Todo: In future this could be a part of CI pipelines or Git hooks.

### Future improvements

Application accessibilty could be considered as a future improvement. Also editing the Todo items could be added.
App could be localized.

