module.exports = {
  preset: 'react-native',
  testEnvironment: 'node',
  testMatch: [
    '<rootDir>/App/**/*.test.{ts,tsx}'
  ],
  collectCoverage: true,
  collectCoverageFrom: [
    '<rootDir>/App/**/*.{ts, tsx}'
  ],
  coverageDirectory: 'coverage',
  coverageReporters: [
    'text-summary',
    'html'
  ],
  moduleFileExtensions: [
    'ts',
    'tsx',
    'js',
    'jsx',
    'json',
    'node'
  ],
  moduleDirectories: [
    'node_modules',
    '<rootDir>/test/utils'
  ],
  transform: {
    '\\.svg$': '<rootDir>/jest-svg-transformer.js',
  },
  transformIgnorePatterns: [
    'node_modules/(?!(jest-)?@?react-native)'
  ],
  setupFilesAfterEnv: [
    '<rootDir>/setup.ts'
  ],
}
