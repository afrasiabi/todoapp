const colors = {
  lavendar: "#E5D9F2",
  magnolia: "#F5EFFF",
  liberty: "#5654B1",
  white: "#fff",
  royalBlue: "#26255F",
  rose: "#F066CD",
  amethyst: "#9C63CE",
};

export default colors;
