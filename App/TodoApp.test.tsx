import { render, screen } from "@testing-library/react-native";
import TodoApp from "./TodoApp";

describe("Todo app", () => {
  it("renders correctly and match snapshot", () => {
    render(<TodoApp />);
    expect(screen.toJSON()).toMatchSnapshot();
  });
});
