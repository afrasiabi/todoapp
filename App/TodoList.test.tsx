import { render, screen, fireEvent } from "@testing-library/react-native";
import TodoList, { Props } from "./TodoList";

const props: Props = {
  todos: [],
  onCompletedToggle: () => {},
  onImportantToggle: () => {},
  onRemove: () => {},
};

describe("Todo list", () => {
  it("renders correctly and match snapshot", () => {
    render(<TodoList {...props} />);
    expect(screen.toJSON()).toMatchSnapshot();
  });

  it("renders todo contents correctly", () => {
    render(
      <TodoList
        {...props}
        todos={[
          { content: "todo1", isCompleted: false, isImportant: false },
          { content: "todo2", isCompleted: false, isImportant: false },
        ]}
      />
    );
    expect(screen.getByText("todo1")).toBeDefined;
    expect(screen.getByText("todo2")).toBeDefined;
  });

  it("should call todo callbacks with correct index", () => {
    const handleImportantToggle = jest.fn();
    const handleCompletedToggle = jest.fn();
    const handleRemove = jest.fn();

    render(
      <TodoList
        {...props}
        todos={[
          { content: "todo1", isCompleted: false, isImportant: false },
          { content: "todo2", isCompleted: false, isImportant: false },
        ]}
        onImportantToggle={handleImportantToggle}
        onCompletedToggle={handleCompletedToggle}
        onRemove={handleRemove}
      />
    );
    fireEvent.press(screen.getAllByTestId("isImportantToggle")[1]);
    expect(handleImportantToggle).toBeCalledWith(1);

    fireEvent.press(screen.getAllByTestId("isCompletedToggle")[0]);
    expect(handleCompletedToggle).toBeCalledWith(0);

    fireEvent.press(screen.getAllByTestId("removeButton")[1]);
    expect(handleRemove).toBeCalledWith(1);
  });
});
