import React from "react";
import { StyleSheet, View, Text, TouchableOpacity, Image } from "react-native";
import colors from "./styles/colors";
import { Todo } from "./types";
import StarIcon from "../assets/icon_star.svg";
import TrashIcon from "../assets/icon_trash.svg";

export type Props = {
  todo: Todo;
  onImportantToggle: () => void;
  onCompletedToggle: () => void;
  onRemove: () => void;
};

const TodoItem: React.FC<Props> = ({
  todo,
  onImportantToggle,
  onCompletedToggle,
  onRemove,
}: Props) => (
  <View style={styles.todoItem}>
    <View style={styles.leftElements}>
      <TouchableOpacity
        testID="isImportantToggle"
        onPress={onImportantToggle}
        style={styles.starIcon}
      >
        <StarIcon
          fill={todo.isImportant ? colors.rose : colors.white}
          width="28"
          height="28"
        />
      </TouchableOpacity>
      <TouchableOpacity
        testID="isCompletedToggle"
        style={[
          styles.isCompletedBtn,
          todo.isCompleted ? styles.isCompleted : styles.isOpen,
        ]}
        onPress={onCompletedToggle}
      />

      <Text
        style={[
          styles.todoContent,
          todo.isCompleted && styles.lineThroughContent,
        ]}
      >
        {todo.content}
      </Text>
    </View>
    <TouchableOpacity
      testID="removeButton"
      onPress={onRemove}
      style={styles.trashIcon}
    >
      <TrashIcon fill={colors.royalBlue} width="24" height="24" />
    </TouchableOpacity>
  </View>
);

const styles = StyleSheet.create({
  todoItem: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: colors.lavendar,
    justifyContent: "space-between",
    padding: 16,
    borderRadius: 10,
    marginBottom: 16,
  },
  leftElements: {
    flexDirection: "row",
    alignItems: "center",
  },
  isCompletedBtn: {
    borderRadius: 50,
    width: 24,
    height: 24,
    marginRight: 8,
  },
  isOpen: {
    backgroundColor: colors.white,
  },
  isCompleted: {
    backgroundColor: colors.amethyst,
  },
  todoContent: {
    maxWidth: "60%",
  },
  lineThroughContent: {
    textDecorationLine: "line-through",
    textDecorationStyle: "solid",
  },
  starIcon: {
    marginRight: 8,
  },
  trashIcon: {
    opacity: 0.5,
  },
});

export default TodoItem;
