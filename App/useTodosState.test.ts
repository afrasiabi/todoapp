import { renderHook, act } from "@testing-library/react-native";
import { useTodosState } from "./useTodosState";

describe("useTodosState", () => {
  it("use", () => {
    const { result } = renderHook(() => useTodosState());
    expect(result.current.todos).toEqual([]);
    act(() => {
      result.current.handleAdd("todo1");
    });
    expect(result.current.todos).toEqual([
      { content: "todo1", isCompleted: false, isImportant: false },
    ]);

    act(() => {
      result.current.handleCompletedToggle(0);
    });
    expect(result.current.todos).toEqual([
      { content: "todo1", isCompleted: true, isImportant: false },
    ]);

    act(() => {
      result.current.handleImportantToggle(0);
    });
    expect(result.current.todos).toEqual([
      { content: "todo1", isImportant: true, isCompleted: true },
    ]);
  });
});
