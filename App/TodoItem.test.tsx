import { render, screen, fireEvent } from "@testing-library/react-native";
import TodoItem, { Props } from "./TodoItem";

const props: Props = {
  todo: {
    content: "abc",
    isCompleted: false,
    isImportant: false,
  },
  onImportantToggle: () => {},
  onCompletedToggle: () => {},
  onRemove: () => {},
};

describe("Todo item", () => {
  it("renders correctly and match snapshot", () => {
    render(<TodoItem {...props} />);
    expect(screen.toJSON()).toMatchSnapshot();
  });

  it("renders correctly and match snapshot when todo is completed", () => {
    render(<TodoItem {...props} todo={{ ...props.todo, isCompleted: true }} />);
    expect(screen.toJSON()).toMatchSnapshot();
  });

  it("renders correctly and match snapshot when todo is important", () => {
    render(<TodoItem {...props} todo={{ ...props.todo, isImportant: true }} />);
    expect(screen.toJSON()).toMatchSnapshot();
  });

  it("should call onImportantToggle correctly", () => {
    const handleImportantToggle = jest.fn();
    render(<TodoItem {...props} onImportantToggle={handleImportantToggle} />);
    fireEvent.press(screen.getByTestId("isImportantToggle"));
    expect(handleImportantToggle).toBeCalledTimes(1);
  });

  it("should call onCompletedToggle correctly", () => {
    const handleCompletedToggle = jest.fn();
    render(<TodoItem {...props} onCompletedToggle={handleCompletedToggle} />);
    fireEvent.press(screen.getByTestId("isCompletedToggle"));
    expect(handleCompletedToggle).toBeCalledTimes(1);
  });

  it("should call onRemove correctly", () => {
    const handleRemove = jest.fn();
    render(<TodoItem {...props} onRemove={handleRemove} />);
    fireEvent.press(screen.getByTestId("removeButton"));
    expect(handleRemove).toBeCalledTimes(1);
  });
});
