import { render, screen, fireEvent } from "@testing-library/react-native";
import TodoEditor, { Props } from "./TodoEditor";

const props: Props = {
  onSubmit: jest.fn(),
  initialValue: "",
};

describe("Todo editor", () => {
  it("renders correctly and match snapshot", () => {
    render(<TodoEditor {...props} />);
    expect(screen.toJSON()).toMatchSnapshot();
  });

  it("calls onSubmit correctly", () => {
    const handleSubmit = jest.fn();
    const expectedContent = "example Todo!";
    render(<TodoEditor {...props} onSubmit={handleSubmit} />);
    fireEvent.changeText(
      screen.getByTestId("todoContentInput"),
      " " + expectedContent + "  "
    );
    fireEvent.press(screen.getByTestId("submitButton"));
    expect(handleSubmit).toBeCalledWith(expectedContent);
    expect(handleSubmit).toBeCalledTimes(1);
  });

  it("doesn't call onSubmit when there is no input", () => {
    const handleSubmit = jest.fn();
    const expectedContent = "";
    render(<TodoEditor {...props} onSubmit={handleSubmit} />);
    fireEvent.changeText(
      screen.getByTestId("todoContentInput"),
      expectedContent
    );
    fireEvent.press(screen.getByTestId("submitButton"));
    expect(handleSubmit).toBeCalledTimes(0);
  });
});
