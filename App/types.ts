export type Todo = {
  content: string;
  isCompleted: boolean;
  isImportant: boolean;
};
