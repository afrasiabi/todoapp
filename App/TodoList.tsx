import React from "react";
import { StyleSheet, ScrollView } from "react-native";
import TodoItem from "./TodoItem";
import { Todo } from "./types";

export type Props = {
  todos: Todo[];
  onCompletedToggle: (index: number) => void;
  onImportantToggle: (index: number) => void;
  onRemove: (index: number) => void;
};

const TodoList: React.FC<Props> = ({
  todos,
  onCompletedToggle,
  onImportantToggle,
  onRemove,
}: Props) => (
  <ScrollView style={styles.todosContainer}>
    {todos.map((todo, index) => {
      return (
        <TodoItem
          key={index}
          todo={todo}
          onCompletedToggle={() => onCompletedToggle(index)}
          onImportantToggle={() => onImportantToggle(index)}
          onRemove={() => onRemove(index)}
        />
      );
    })}
  </ScrollView>
);

const styles = StyleSheet.create({
  todosContainer: {
    marginTop: 16,
  },
});

export default TodoList;
