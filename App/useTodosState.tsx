import { useState } from "react";
import { Todo } from "./types";

type TodoState = {
  todos: Todo[];
  handleAdd: (value: string) => void;
  handleCompletedToggle: (index: number) => void;
  handleImportantToggle: (index: number) => void;
  handleRemove: (index: number) => void;
};

export const useTodosState = (): TodoState => {
  const [todos, setTodos] = useState<Todo[]>([]);

  const handleAdd = (value: string): void => {
    setTodos([
      { content: value, isCompleted: false, isImportant: false },
      ...todos,
    ]);
  };

  const handleCompletedToggle = (index: number): void => {
    setTodos(
      todos.map((todo, i) => {
        if (i === index) {
          return { ...todo, isCompleted: !todo.isCompleted };
        }
        return todo;
      })
    );
  };

  const handleImportantToggle = (index: number): void => {
    setTodos(
      todos.map((todo, i) => {
        if (i === index) {
          return { ...todo, isImportant: !todo.isImportant };
        }
        return todo;
      })
    );
  };

  const handleRemove = (index: number): void => {
    setTodos(todos.filter((item, i) => i !== index));
  };

  return {
    todos,
    handleAdd,
    handleCompletedToggle,
    handleImportantToggle,
    handleRemove,
  };
};
