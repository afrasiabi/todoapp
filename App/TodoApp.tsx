import React from "react";
import { StyleSheet, View } from "react-native";
import TodoEditor from "./TodoEditor";
import TodoList from "./TodoList";
import { useTodosState } from "./useTodosState";
import colors from "./styles/colors";

const TodoApp: React.FC = () => {
  const {
    todos,
    handleAdd,
    handleCompletedToggle,
    handleImportantToggle,
    handleRemove,
  } = useTodosState();

  return (
    <View style={styles.container}>
      <TodoEditor onSubmit={handleAdd} initialValue="" />
      <TodoList
        todos={todos}
        onCompletedToggle={handleCompletedToggle}
        onImportantToggle={handleImportantToggle}
        onRemove={handleRemove}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 80,
    paddingHorizontal: 24,
    backgroundColor: colors.magnolia,
  },
});

export default TodoApp;
