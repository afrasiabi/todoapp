import React, { useState } from "react";
import {
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Image,
} from "react-native";
import AddIcon from "../assets/icon_add.svg";
import colors from "./styles/colors";

export type Props = {
  onSubmit: (value: string) => void;
  initialValue: string;
};

const TodoEditor: React.FC<Props> = ({ onSubmit, initialValue }: Props) => {
  const [value, setValue] = useState(initialValue);

  const handleSubmit = (): void => {
    const trimmedValue = value.trim();
    if (trimmedValue === "") return;
    setValue("");
    onSubmit(trimmedValue);
  };

  return (
    <View>
      <View style={styles.todoEditorContainer}>
        <TextInput
          testID="todoContentInput"
          style={styles.input}
          onChangeText={setValue}
          value={value}
          onSubmitEditing={handleSubmit}
          blurOnSubmit={false}
          placeholder="Add a new todo"
        />
        <TouchableOpacity
          testID="submitButton"
          onPress={handleSubmit}
          style={styles.submitButton}
        >
          <AddIcon fill={colors.liberty} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 40,
    borderRadius: 5,
    flex: 1,
    backgroundColor: colors.white,
    padding: 8,
    color: colors.royalBlue,
  },
  todoEditorContainer: {
    flexDirection: "row",
  },
  submitButton: {
    width: 40,
    height: 40,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 4,
  },
});

export default TodoEditor;
