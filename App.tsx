import React from "react";
import TodoApp from "./App/TodoApp";

const App: React.FC = () => <TodoApp />;

export default App;
